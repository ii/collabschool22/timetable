import numpy as np

def get_data(mu, sigma):
    np.random.seed(19680801)
    
    x = mu + sigma * np.random.randn(437)
    return x
    