import random
import numpy as np



N = 1000000

xlst = tuple( random.uniform(-10.,10.) for _ in range(N) )

xarr = np.array(xlst, dtype='float64')

ones = np.ones_like(xarr)










def ranged_forloop(xs):
    total = 0
    for i in xs:
        total += abs(i)
    return total









def indexed_forloop(xs):
    total = 0
    L = len(xs)
    for i in range(L):
        total += abs(xs[i])
    return total




def functional_plain(xs):
    return sum(map(abs,xs))




def functional_numpy(xs):
    return np.sum(np.abs(xs))



def dotprod(xs):
    return np.dot(np.abs(xs), ones)



